'use strict';

var Demo;

function Init() {
	var canvas = document.getElementById('gl-surface');
	var gl = canvas.getContext('webgl');
	if (!gl) {
		console.log('Failed to get WebGL context - trying experimental context');
		gl = canvas.getContext('experimental-webgl');
	}
	if (!gl) {
		alert('Your browser does not support WebGL - please use a different browser\nGoogleChrome works great!');
		return;
	}

	Demo = new BedroomScene(gl);
	Demo.Load(function (demoLoadError) {
		if (demoLoadError) {
			alert('Could not load the demo - see console for more details');
			console.error(demoLoadError);
		} else {
			Demo.Begin();
		}
	});
}

// Change object mode when toggle is clicked
function changeObjectMode() {
    // Get the checkbox
    var checkBox = document.getElementById("objectMode");

    // If the checkbox is checked, object mode is wireframe
    if (checkBox.checked == true){
		Demo.objectMode = "wireframe";
    } else {
		Demo.objectMode = "shading";
    }
}

// Show interactive mode when toggle is clicked
function showInteractiveMode() {
    // Get the checkbox
    var checkBox = document.getElementById("displayMode");

    // If the checkbox is checked, object mode is wireframe
    if (checkBox.checked == true){
		Demo.demoFlag = false;
        document.getElementById('interactiveMode').style.display = 'block';
    } else {
		Demo.demoFlag = true;
		document.getElementById('interactiveMode').style.display = 'none';
    }
}

// Change objects' angles based on user input
function changeOnAngle() {
	Demo.chairAngle = document.getElementById("slider0").value;
}